use crate::uses::*;

pub struct Model {
    submit: ResrcState<FetchTask>,
    input: String,
    id: ID,
    fetch: FetchService,
    link: ComponentLink<Self>
}

impl Component for Model {
    type Message = Msg<String, ResrcAction<()>>;
    type Properties = ID;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Model {
            submit: NotLoaded(None),
            input: String::new(),
            id: props,
            fetch: FetchService::new(),
            link
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Gui(input) => self.input = input,
            Resource(Load) => self.submit = NotLoaded(Some(fetch!{self.fetch, self.link,
                post, "/auth", Json(&self.input), |response: Response<Text>|
                    if response.status().is_success() {
                        Resource(Finish(()))
                    } else {
                        Resource(Fail)
                    }
            })),
            Resource(Finish(())) => {
                self.submit = Loaded;
                self.id.reload.emit(());
            },
            Resource(Fail) => {
                self.submit = Failed
            }
        }
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.id = props;
        true
    }

    fn view(&self) -> Html {container!{"Login",
        <div>
            <div class="py-2">
                <label class="block text-gray-700 font-bold mb-2" for="token">
                    {"Token"}
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 mb-2 \
                text-gray-700 leading-tight focus:outline-none focus:shadow-outline font-mono"
                id="token" type="text" placeholder="************"
                oninput=self.link.callback(|input: InputData| Gui(input.value)) />
                <div class="flex flex-row mb-2">
                    <button class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 \
                    px-4 rounded focus:outline-none focus:shadow-outline mr-3"
                    onclick=self.link.callback(|_| Resource(Load))>
                        {"Submit"}
                    </button>
                    <div class="flex h-10 w-10 text-3xl justify-center items-center">{
                        match self.submit {
                            NotLoaded(Some(..)) => html!{
                                <i class="fas fa-spinner fa-spin text-blue-500"></i>
                            },
                            NotLoaded(None) => html!{},
                            Loaded =>
                                html!{<i class="fas fa-check text-green-500"></i>},
                            Failed =>
                                html!{<i class="fas fa-times text-red-500"></i>}
                        }
                    }</div>
                </div>
            </div>

            <div class="mb-2 font-bold text-gray-700">
                {format!("Current ID: {}", match self.id.id {
                    Some(id) => format!("{}", id),
                    None => "None".to_string()
                })}
            </div>
        </div>
    }}
}
