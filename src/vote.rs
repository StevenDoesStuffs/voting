use crate::uses::*;

pub struct Model {
    data: Data,
    state: State,
    link: ComponentLink<Self>,
    internal: Internal
}

#[derive(Default, Serialize, Deserialize)]
pub struct Data {
    candidates: Vec<Candidate>,
    vote: Vec<Vec<usize>>,
    id: usize
}

#[derive(Default)]
pub struct State {
    selected: usize,
    info: Option<usize>,
}

pub struct Internal {
    data_state: ResrcState<FetchTask>,
    submit_state: ResrcState<FetchTask>,
    fetch: FetchService,
    storage: StorageService
}

#[derive(Clone, Copy)]
pub enum Move {
    Top,
    UpOne,
    UpHalf,
    DownHalf,
    DownOne,
    Bottom
}

static MOVE_BUTTONS: [(Move, &str); 6] = [
    (Move::Top, "fa-angle-double-up"),
    (Move::UpOne, "fa-arrow-up"),
    (Move::UpHalf, "text-4xl fa-caret-up"),
    (Move::DownHalf, "text-4xl fa-caret-down"),
    (Move::DownOne, "fa-arrow-down"),
    (Move::Bottom, "fa-angle-double-down")
];

#[derive(Clone, Copy)]
pub enum GuiMsg {
    Move(Move),
    Select(usize),
    Info(Option<usize>),
}
use GuiMsg::*;

pub enum ResrcMsg {
    Candidates(ResrcAction<Vec<Candidate>>),
    Submit(ResrcAction<()>)
}
use ResrcMsg::*;

impl Component for Model {
    type Message = Msg<GuiMsg, ResrcMsg>;
    type Properties = ID;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        link.send_message(Resource(Candidates(Load)));

        Model {
            data: Data {id: props.id.unwrap_or(0), ..Default::default()},
            state: State::default(),
            link,
            internal: Internal {
                data_state: NotLoaded(None),
                submit_state: NotLoaded(None),
                fetch: FetchService::new(),
                storage: StorageService::new(storage::Area::Local)
                    .expect("Failure initializing local storage")
            }
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Gui(Move(action)) => self.update_move(action),

            Gui(Select(id)) =>
                self.state.selected = id,

            Gui(Info(selected)) =>
                self.state.info = selected,

            Resource(Candidates(action)) => match action {
                Finish(candidates) => {
                    self.data.vote = vec![(0..candidates.len()).collect()];
                    if let Json(Ok(data)) = self.internal.storage
                            .restore::<Json<Result<Data>>>("vote_data") {

                        if candidates == data.candidates && self.data.id == data.id {
                            self.data.vote = data.vote;
                        }
                    }

                    self.data.candidates = candidates;
                    self.internal.data_state = Loaded;
                },
                Load => {
                    self.internal.data_state = NotLoaded(Some(get!{self.internal.fetch,
                        self.link, "/candidates", |id: Result<Vec<Candidate>>| match id {
                            Ok(candidates) => Resource(Candidates(Finish(candidates))),
                            Err(..) => Resource(Candidates(Fail))
                        }
                    }));
                    return true;
                },
                Fail => {
                    self.internal.data_state = Failed;
                    self.data.candidates = vec![];
                }
            },

            Resource(Submit(action)) => match action {
                Finish(()) =>
                    self.internal.submit_state = Loaded,

                Load => self.internal.submit_state = NotLoaded(Some(fetch!{self.internal.fetch,
                    self.link, put, &format!("/voters/{}", self.data.id), Json(&self.data.vote),

                    |response: Response<Text>| if response.status().is_success() {
                        Resource(Submit(Finish(())))
                    } else {
                        Resource(Submit(Fail))
                    }
                })),

                Fail =>
                    self.internal.submit_state = Failed
            }
        };
        if self.data.id != 0 {
            self.internal.storage.store("vote_data", Json(&self.data));
        }
        true
    }
    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.data.id = props.id.unwrap_or(0);
        self.data.vote = vec![(0..self.data.candidates.len()).collect()];
        if let Json(Ok(data)) = self.internal.storage
            .restore::<Json<Result<Data>>>("vote_data") {

            if self.data.candidates == data.candidates && self.data.id == data.id {
                self.data.vote = data.vote;
            }
        }
        true
    }

    fn view(&self) -> Html {
        match (&self.internal.data_state, &self.state.info) {
            (Loaded, None) => container!{"Vote",
                <div class="flex flex-row-reverse">
                    <div class="w-10 flex-shrink-0 ml-4"></div>
                    <div class="flex flex-col flex-grow">
                        {for self.data.vote.iter().map(|row| self.view_row(row))}
                        <div class="flex flex-row my-4">
                            <button class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 \
                            px-4 rounded focus:outline-none focus:shadow-outline mr-3"
                            onclick=self.link.callback(|_| Resource(Submit(Load)))>
                                {"Submit"}
                            </button>
                            <div class="flex h-10 w-10 text-3xl justify-center items-center">{
                                match self.internal.submit_state {
                                    NotLoaded(Some(..)) => html!{
                                        <i class="fas fa-spinner fa-spin text-blue-500"></i>
                                    },
                                    NotLoaded(None) => html!{},
                                    Loaded =>
                                        html!{<i class="fas fa-check text-green-500"></i>},
                                    Failed =>
                                        html!{<i class="fas fa-times text-red-500"></i>}
                                }
                            }</div>
                        </div>
                    </div>
                    <div class="flex flex-col items-center text-3xl text-white my-2 fixed">{
                        for MOVE_BUTTONS.iter()
                            .map(|button| self.view_nav_button(Move(button.0), button.1))
                    }</div>
                </div>
            },
            (Loaded, Some(id)) => container!{self.data.candidates[*id].name.to_owned(),
                <div class="flex flex-row-reverse">
                    <div class="w-10 flex-shrink-0 ml-4"></div>
                    <div class="flex flex-col flex-grow">
                        {self.data.candidates[*id].desc.to_owned()}
                    </div>
                    <div class="flex flex-col items-center text-3xl text-white my-2 fixed">
                        {self.view_nav_button(Info(None), "fa-arrow-left")}
                    </div>
                </div>
            },
            (NotLoaded(..), ..) => loader!(),
            (Failed, ..) => error!()
        }
    }
}
// code omitted because it's too long
impl Model {
    fn remove_vote(&mut self, id: usize) -> usize {
        for i in 0..self.data.vote.len() {
            if let Some(index) = self.data.vote[i].iter()
                    .position(|c| *c == id) {
                self.data.vote[i].remove(index);
                return i
            }
        }
        unreachable!();
    }

    fn update_move(&mut self, action: Move) {
        match action {
            Move::UpOne => {
                self.update(Gui(Move(Move::UpHalf)));
                self.update(Gui(Move(Move::UpHalf)));
                return;
            },
            Move::DownOne => {
                self.update(Gui(Move(Move::DownHalf)));
                self.update(Gui(Move(Move::DownHalf)));
                return;
            }, _ => {}
        }

        let prev = self.remove_vote(self.state.selected);
        match action {
            Move::Top =>
                self.data.vote.insert(0, vec![self.state.selected]),
            Move::Bottom =>
                self.data.vote.push(vec![self.state.selected]),
            Move::UpHalf =>
                if prev == 0 {self.data.vote.insert(0, vec![self.state.selected])}
                else if self.data.vote[prev].is_empty() {
                    self.data.vote[prev - 1].push(self.state.selected);
                } else {
                    self.data.vote.insert(prev, vec![self.state.selected])
                },
            Move::DownHalf =>
                if prev == self.data.vote.len() - 1 {
                    self.data.vote.push(vec![self.state.selected])
                } else if self.data.vote[prev].is_empty() {
                    self.data.vote[prev + 1].push(self.state.selected);
                } else {
                    self.data.vote.insert(prev + 1, vec![self.state.selected])
                },
            _ => {}
        }
        self.data.vote.retain(|row| !row.is_empty());
    }
}

impl Model {

    fn append_selected(&self, base: &str, candidate: &Candidate) -> String {
        base.to_owned() + " " + if self.state.selected == candidate.id {
            "outline-none shadow-outline bg-blue-600"
        } else {
            "bg-gray-900 hover:bg-gray-700 hover:text-white t-all hover:shadow-xl"
        }
    }

    fn view_nav_button(&self, action: GuiMsg, class: &str) -> Html {html!{
        <a class="flex justify-center items-center bg-orange-500 cursor-pointer \
        rounded mb-1 w-10 h-10 shadow-lg hover:shadow-xl hover:bg-orange-400 t-all"
        onclick=self.link.callback(move |_| Gui(action))>
            <i class="fas".to_owned() + " " + class></i>
        </a>
    }}

    fn view_row(&self, row: &[usize]) -> Html {html!{
        <div class="flex flex-row my-2">
            <div class="flex flex-row flex-wrap"> {
                for row.iter().map(|candidate|
                    self.view_candidate(&self.data.candidates[*candidate]))
            }</div>
            <div class="flex flex-row flex-wrap">
            </div>
        </div>
    }}

    fn view_candidate(&self, candidate: &Candidate) -> Html {
        let id = candidate.id; // big dumb, lifetimes why
        html!{
            <div class=self.append_selected(
                "flex flex-row items-center text-gray-300 \
                mb-1 mr-1 rounded shadow-lg",
                candidate
            )>
                <div class="text-md p-3 pr-0 cursor-pointer"
                onclick=self.link.callback(move |_| Gui(Select(id)))>
                    {candidate.name.to_owned()}
                </div>
                <a class="p-3 cursor-pointer" onclick=self.link.callback(move |_| Gui(Info(Some(id))))>
                    <i class="fas fa-info-circle text-md font-normal"></i>
                </a>
            </div>
        }
    }
}
