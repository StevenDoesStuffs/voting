#![recursion_limit = "8192"]

mod uses;
use crate::uses::*;

#[macro_use]
mod util;

mod vote;
mod navbar;
mod login;
mod result;

pub struct Model {
    page: Page,
    id: Option<usize>,
    id_state: ResrcState<FetchTask>,
    fetch: FetchService,
    link: ComponentLink<Self>,
    reload_id: Callback<()>
}

impl Component for Model {
    type Message = Msg<Page, ResrcAction<usize>>;
    type Properties = ();

    fn create(_: Self::Properties, mut link: ComponentLink<Self>) -> Self {
        register_routing(&mut link, route, Gui, || Page::NotFound);

        link.send_message(Resource(Load));
        Model {
            page: match Url::parse(&window().location().href().unwrap()) {
                Ok(url) => route(url.fragment().unwrap_or_default()),
                Err(..) => Page::NotFound
            },
            id: None,
            id_state: NotLoaded(None),
            fetch: FetchService::new(),
            reload_id: link.callback(|_| Resource(Load)),
            link
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Gui(page) => self.page = page,
            Resource(Load) => self.id_state = NotLoaded(Some(get!{
                self.fetch, self.link, "/auth", |id: Result<usize>| match id {
                    Ok(id) => Resource(Finish(id)),
                    Err(..) => Resource(Fail)
                }
            })),
            Resource(Finish(id)) => {
                self.id = Some(id);
                self.id_state = Loaded;
            },
            Resource(Fail) => {
                self.id = None;
                self.id_state = Failed;
            }
        }
        true
    }

    fn view(&self) -> Html {html!{
        <div class="flex flex-col font-light h-full">
            <navbar::Model />
            {match self.page {
                Page::Vote => {
                    if let Failed = self.id_state {
                        window().location().set_hash("#login")
                            .expect("Failed to change window hash");
                    }
                    let vote =
                        html!{<vote::Model id=self.id reload=self.reload_id.clone() />};
                    vote
                },
                Page::Login =>
                    html!{<login::Model id=self.id reload=self.reload_id.clone() />},
                Page::Results =>
                    html!{<result::Model />},
                Page::NotFound => container!{"404 Not Found", <div></div>}
            }}
        </div>
    }}
}

fn route(hash: &str) -> Page {
    match hash {
        "" | "vote" => Page::Vote,
        "login" => Page::Login,
        "results" => Page::Results,
        _ => Page::NotFound
    }
}

#[wasm_bindgen(start)]
pub fn main() {
    console_error_panic_hook::set_once();

    yew::initialize();
    App::<Model>::new().mount(document()
        .get_element_by_id("main")
        .unwrap()
    );
    yew::run_loop();
}
