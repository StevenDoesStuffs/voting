use crate::uses::*;

#[derive(Serialize, Deserialize, Default)]
pub struct VoteResult {
    places: Vec<u32>,
    pairs: Vec<Vec<u32>>,
    paths: Vec<Vec<u32>>,
    wins: Vec<u32>
}

pub struct Model {
    result: VoteResult,
    result_state: ResrcState<FetchTask>,
    candidates: Vec<Candidate>,
    candidates_state: ResrcState<FetchTask>,
    fetch: FetchService,
    link: ComponentLink<Self>
}


pub enum ResrcMsg {
    Candidates(ResrcAction<Vec<Candidate>>),
    ResultMsg(ResrcAction<VoteResult>)
}
use ResrcMsg::*;

impl Component for Model {
    type Message = Msg<(), ResrcMsg>;
    type Properties = ();
    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        link.send_message(Resource(Candidates(Load)));
        link.send_message(Resource(ResultMsg(Load)));
        Model {
            result: Default::default(),
            candidates: Default::default(),
            result_state: NotLoaded(None),
            candidates_state: NotLoaded(None),
            fetch: FetchService::new(),
            link
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Gui(()) => unreachable!(),
            Resource(Candidates(action)) => get_action!{
                action, self.link, self.fetch, "/candidates",
                Vec<Candidate>, self.candidates, self.candidates_state, Candidates
            },
            Resource(ResultMsg(action)) => get_action!{
                action, self.link, self.fetch, "/result",
                VoteResult, self.result, self.result_state, ResultMsg
            }
        }
        true
    }

    fn view(&self) -> Html {
        match (&self.candidates_state, &self.result_state) {
            (Failed, _) | (_, Failed) => error!(),
            (NotLoaded(..), _) | (_, NotLoaded(..)) => loader!(),
            _ => container!{"Results", {html!{
                <div>
                    <ol class="list-decimal list-inside mt-2 mb-6">{
                        for self.result.places.iter().map(|id| html!{
                            <li class="my-2">{self.candidates[*id as usize].name.to_string()}</li>
                        })
                    }</ol>
                    <div class="flex flex-row">
                        <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 \
                        px-4 rounded focus:outline-none focus:shadow-outline mr-2"
                        href="/api/candidates">
                            {"Candidates Data"}
                        </a>
                        <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 \
                        px-4 rounded focus:outline-none focus:shadow-outline mr-2"
                        href="/api/result">
                            {"Result Data"}
                        </a>
                        <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 \
                        px-4 rounded focus:outline-none focus:shadow-outline mr-2"
                        href="/api/voters">
                            {"Voters Data"}
                        </a>
                    </div>
                </div>
            }}}
        }
    }
}
