use crate::uses::*;

pub type Result<T> = std::result::Result<T, Error>;

pub static BACKEND: &str = "/api";

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct Candidate {
    pub id: usize,
    pub name: String,
    pub desc: String
}

pub enum ResrcState<T> {
    NotLoaded(Option<T>),
    Loaded,
    Failed
}
pub use ResrcState::*;

pub enum ResrcAction<T> {
    Load,
    Finish(T),
    Fail
}
pub use ResrcAction::*;

pub enum Msg<G, R> {
    Gui(G),
    Resource(R)
}
pub use Msg::*;

#[derive(PartialEq, Eq)]
pub enum Page {
    Vote,
    Login,
    Results,
    NotFound
}

#[derive(Clone, Properties)]
pub struct ID {
    pub id: Option<usize>,
    pub reload: Callback<()>
}

pub fn register_routing<COMP: Component + Renderable, PAGE: 'static>(
        link: &mut ComponentLink<COMP>,
        route: impl Fn(&str) -> PAGE + 'static,
        msg: impl Fn(PAGE) -> COMP::Message + 'static,
        err: impl Fn() -> PAGE + 'static) {

    let cb = link.callback(move |page| msg(page));
    EventListener::new(&window(), "hashchange", move |event: &web::Event| {
        let event = event.dyn_ref::<web::HashChangeEvent>().unwrap_throw();
        cb.emit(match Url::parse(&event.new_url()) {
            Ok(url) => route(url.fragment().unwrap_or_default()),
            Err(..) => err()
        })
    }).forget();
}

#[macro_export]
macro_rules! blank {
    ($($stuff: tt)+) => {html!{
        <div class="flex-grow container mx-auto bg-white px-5 py-4" style="max-width: 840px">
            $($stuff)+
        </div>
    }};
}
#[macro_export]
macro_rules! loader {
    () => {blank!{
        <div class="flex h-full items-center justify-center">
            <div class="loader"></div>
        </div>
    }};
}
#[macro_export]
macro_rules! error {
    () => {blank!{
        <div class="flex h-full items-center justify-center">
            <i class="fas fa-times error"></i>
        </div>
    }};
}
#[macro_export]
macro_rules! container {
    ($name: expr, $($stuff: tt)+) => {blank!{
        <p class="text-2xl">{$name}</p>
        <hr class="my-3" />
        $($stuff)+
    }};
}

#[macro_export]
macro_rules! fetch {
    ($fetch: expr, $link: expr, $req: ident, $loc: expr,
    $body: expr, |$i: ident: $ty: ty| $handle: expr) => {
        $fetch.fetch(
            Request::$req(BACKEND.to_owned() + $loc).body($body).unwrap(),
            $link.callback(|$i: $ty| {
                $handle
            }
        )).expect("Internal fetch error");
    };
}
#[macro_export]
macro_rules! fetch_json {
    ($fetch: expr, $link: expr, $req: ident, $loc: expr,
    $body: expr, |$i: ident: $ty: ty| $handle: expr) => {
        fetch!{$fetch, $link, $req, $loc, $body, |$i: Response<Json<$ty>>| {
            let Json($i) = $i.into_body();
            $handle
        }};
    };
}
#[macro_export]
macro_rules! get {
    ($fetch: expr, $link: expr, $loc: expr, |$i: ident: $ty: ty| $handle: expr) => {
        fetch_json!{$fetch, $link, get, $loc, Nothing, |$i: $ty| $handle};
    };
}


macro_rules! get_action {
    ($action: expr, $link: expr, $fetch: expr, $loc: expr, $ty: ty,
    $resrc: expr, $resrc_state: expr, $name: ident) => {
        match $action {
            Finish(x) => {
                $resrc = x;
                $resrc_state = Loaded;
            },
            Load => $resrc_state = NotLoaded(Some(get!{$fetch,
                $link, $loc, |x: Result<$ty>| match x {
                    Ok(x) => Resource($name(Finish(x))),
                    Err(..) => Resource($name(Fail))
                }
            })),
            Fail => {
                $resrc = Default::default();
                $resrc_state = Failed;
            }
        }
    };
}

pub fn window() -> web::Window {
    web::window().unwrap()
}

pub fn document() -> web::Document {
    window().document().unwrap()
}
