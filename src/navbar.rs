use crate::uses::*;

struct NavLink {
    name: String,
    dest: String
}

impl NavLink {
    fn new(name: &str, dest: &str) -> NavLink {
        NavLink {
            name: name.to_owned(),
            dest: dest.to_owned()
        }
    }
}

pub struct Model {
    expand: bool,
    links: Vec<NavLink>,
    name: String,
    name_state: ResrcState<FetchTask>,
    fetch: FetchService,
    link: ComponentLink<Self>
}

impl Model {
    fn append_tag(&self, base: &str, is_expand: bool, tag: &str) -> String {
        base.to_owned() + " " + if !(self.expand ^ is_expand) {tag} else {""}
    }
}


impl Component for Model {
    type Message = Msg<(), ResrcAction<String>>;
    type Properties = ();
    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        link.send_message(Resource(Load));
        Model {
            expand: false,
            links: vec![
                NavLink::new("Login", "#login"),
                NavLink::new("Vote", "#vote"),
                NavLink::new("Results", "#results"),
                NavLink::new("Github", "https://gitlab.com/StevenDoesStuffs")
            ],
            name: "LOADING...".to_string(),
            name_state: NotLoaded(None),
            fetch: FetchService::new(),
            link
        }
    }
    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Gui(..) => self.expand = !self.expand,
            Resource(Load) => self.name_state = NotLoaded(Some(get!{
                self.fetch, self.link, "/name",
                |name: Result<String>| match name {
                    Ok(name) => Resource(Finish(name)),
                    Err(..) => Resource(Fail)
                }
            })),
            Resource(Finish(name)) => {
                self.name = name;
                self.name_state = Loaded;
            },
            Resource(Fail) => {
                self.name = "FAILED".to_string();
                self.name_state = Failed;
            }
        }
        true
    }

    fn view(&self) -> Html {html!{

        <nav class="sticky top-0 bg-gray-900 text-gray-300 shadow-xl">
        <div
            class="flex flex-col sm:flex-row container mx-auto"
            style="max-width: 840px"
        >
            <div class="flex flex-row flex-grow items-center">

                <div class="flex flex-grow">
                    <p class="text-xl font-normal p-5">
                        { self.name.to_string() }
                    </p>
                </div>
                <div class="flex sm:hidden">
                    <a class="p-5 cursor-pointer" onclick=self.link.callback(|_| Gui(()))>
                        <i class="fas fa-bars text-2xl"
                        style="height: 1.5rem; width: 1.5rem"></i>
                    </a>
                </div>
            </div>

            <div class=self.append_tag("sm:hidden px-5 items-center pb-5", false, "hidden")>{
                for self.links.iter().map(|link| html! {
                    <a class="hover:text-white t-all mr-5 py-2" href=link.dest.to_string()>
                        { &link.name }
                    </a>
                })
            }</div>

            <div class="hidden sm:flex px-5 py-3 items-center">{
                for self.links.iter().map(|link| html! {
                    <a class="hover:text-white t-all ml-5 py-2" href=link.dest.to_string()>
                        { &link.name }
                    </a>
                })
            }</div>

        </div></nav>
    }}
}
